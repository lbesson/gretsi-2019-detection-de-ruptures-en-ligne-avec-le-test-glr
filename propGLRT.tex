\subsection{Properties of the GLR}\label{subsec:PropGLR}

\cite{Maillard2018GLR} studies in depth the properties of the GLR with divergence $d(x,y) = (x-y)/(2\sigma^2)$ for distributions that are $\sigma^2$ sub-Gaussian. In this paper we provide a control of the false alarm probability (Lemma~\ref{lem:FalseAlarm}) for the GLR based on any divergence function $d$ based on data satisfying \eqref{eq:subExpo} for the exponential family with divergence $d$. Then, we propose in Lemma~\ref{lem:Delay} a simple control of the delay when  the Bernoulli-GLR change point detectors is used for bounded distributions. 

In Lemma~\ref{lem:FalseAlarm} below, we propose a choice of the threshold function $\beta(t,\delta)$ under which the probability that there exists a false alarm under \iid{} data is small. To define $\beta$, we need to introduce the function $\cT$,
\begin{equation}\label{def:T}
    \cT(x) ~:=~ 2 \tilde h\left(\frac{h^{-1}(1+x) + \ln(2\zeta(2))}{2}\right)
\end{equation}
where for $u \ge 1$ we define $h(u) = u - \ln(u)$ and its inverse $h^{-1}(u)$.
And for any $x \ge 0$
\[ \tilde h(x) ~=~ \begin{cases}
    e^{1/h^{-1}(x)} h^{-1}(x) & \text{if $x \ge h^{-1}(1/\ln (3/2))$,}
    \\
    (3/2) (x-\ln \ln (3/2))
    & \text{otherwise.}
\end{cases} \]
%
The function $\cT$ is easy to compute numerically.
% The inverse $h^{-1}$ can be computed using $\mathcal{W}$, the Lambert $\mathcal{W}$ function \citep{Corless96} (inverse of $x \mapsto x \exp(x)$), as $h^{-1}(x) = - \mathcal{W}(- \exp(-x))$.
Its use for the construction of concentration inequalities that are uniform in time is detailed in~\cite{KK18Martingales}, where tight upper bound on the function $\cT$ are also given:  $\cT (x) \simeq x + 4 \ln\big(1 + x + \sqrt{2x}\big)$ for $x\geq 5$ and $\cT(x) \sim x$ when $x$ is large. The proof of Lemma~\ref{lem:FalseAlarm} is given in Appendix~\ref{proof:FalseAlarm}, it relies on the use of mixture martingales recently promoted by \cite{KK18Martingales}.

\begin{lemma}\label{lem:FalseAlarm}
    Assume that $(X_i)$ is i.i.d. from distribution $\nu$ with mean $\mu_0$ and that $\nu$ satisfies \eqref{eq:subExpo} for an exponential family with divergence function $d$. The GLR stopping rule $T_\delta$ defined in \eqref{def:GLR} using the divergence $d$ and based on the threshold function
    \begin{equation}\beta(t,\delta)= 2\cT\left(\frac{\ln(3t\sqrt{t}/\delta)}{2}\right) + 6\ln(1+\ln(t))\label{def:beta}\end{equation}
    satisfies $\bP(T_\delta < \infty) \leq \delta$.
\end{lemma}

We now turn our attention to GLR change-point detectors when the observations $(X_t)$ have bounded support in $[0,1]$. From Lemma~\ref{lem:FalseAlarm}, the Bernoulli GLR (i.e., the GLR with divergence $\kl(x,y)$) with a threshold function given by \eqref{def:beta} has a  probability of false alarm upper bounded by $\delta$. We now provide in Lemma~\ref{lem:Delay} a control of the \emph{detection delay} for this test.

\begin{lemma}\label{lem:Delay} Let $\bP_{\mu_0,\mu_1,\tau}$ denote a probabilistic model under which $X_t \in [0,1]$ and for all $t \leq \tau$, $X_t$ has mean $\mu_0$ and for  $t > \tau$, it has mean $\mu_1$, with $\mu_0 \neq \mu_1$. We introduce $\Delta := \mu_0 - \mu_1$. If $T_\delta$ is the GLR change point detector \eqref{def:GLR} based on either $d(x,y) = 2(x-y)^2$ or $d(x,y) = \kl(x,y)$, then 
    \[\bP_{\mu_0,\mu_1,\tau}\Big(T_{\delta} \geq \tau + u\Big)\leq \exp\left(-\frac{2\tau u}{\tau + u}\left(\left[|\Delta| - \sqrt{\frac{\tau + u}{2\tau u} \beta(\tau + u,\delta)}\right]_+\right)^2\right).\]
\end{lemma}

\TODO{Define this $x_+ = \max(0, x)$ notation before using it.}

A consequence of this result is that if the change-point $\tau$ satisfies $\tau \geq ({2(1+c)}/{c \Delta^2}) \beta((1+c)\tau,\delta)$, which can be shown to occurs for $\tau \simeq \frac{C}{\Delta^2}\ln(1/\delta)$ for some constant $C$ for the choice of $\beta$ \eqref{def:beta}, then \[\bP(T_\delta \geq (1+c)\tau) \leq \exp\left(- \tau \frac{\Delta^2 c}{2(1+c)}\right)\cdot\]
This suggests that a change-point is detected with high probability if we have $O(\Delta^{-2}\ln(1/\delta))$ samples before and after the change-point. The proof of Lemma~\ref{lem:Delay} is provided in Appendix~\ref{proof:Delay}. For the Bernoulli GLR, it relies on Pinsker's inequality and on the following deviation inequality, which will also be useful in the analysis of our bandit algorithm. Lemma~\ref{lem:Chernoff2arms} relies on a simple application of the Cram\'er-Chernoff method and is proved in Appendix~\ref{proof:Chernoff2arms}.


\begin{lemma}\label{lem:Chernoff2arms}
    Let $\hat{\mu}_{i,s}$ be the empirical mean of $s$ i.i.d. observations with mean $\mu_i$, $i \in \{a,b\}$, that are $\sigma^2$-sub-Gaussian. Define $\Delta = \mu_a - \mu_b$.
    \begin{eqnarray*}
        \bP\left(\frac{sr}{s+r}\Big(\hat{\mu}_{a,s} - \hat{\mu}_{b,r} - \Delta\Big)^2 \geq u \right) &\leq& 2\exp\left(- \frac{u}{2\sigma^2}\right)\cdot
    \end{eqnarray*}
\end{lemma}
